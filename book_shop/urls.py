from django.urls import path

from book_shop.views import PublisherBooksShops, MarkBookAsSold

urlpatterns = [
    path('publishers/<int:pk>/', PublisherBooksShops.as_view(), name='publisher.shops_list'),
    path('shops/<int:shop_id>/books/<int:book_id>/', MarkBookAsSold.as_view(), name='shop.mark_book_as_sold')
]