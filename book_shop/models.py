from django.db import models


# Create your models here.
class Publisher(models.Model):
    name = models.CharField(max_length=30, verbose_name='Name')

    class Meta:
        verbose_name = 'Publisher'
        verbose_name_plural = 'Publishers'

    def __str__(self):
        return self.name


class Book(models.Model):
    title = models.CharField(max_length=100, verbose_name='Title')
    publisher = models.ForeignKey(Publisher, verbose_name='Publisher', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Book'
        verbose_name_plural = 'Books'

    def __str__(self):
        return self.title


class Shop(models.Model):
    name = models.CharField(max_length=30, verbose_name='Name')
    books_sold = models.IntegerField(default=0, verbose_name='Books sold')
    books = models.ManyToManyField(Book, through='BookInShop')

    class Meta:
        verbose_name = 'Shop'
        verbose_name_plural = 'Shops'

    def __str__(self):
        return self.name


class BookInShop(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE)
    copies_in_stock = models.IntegerField()

    class Meta:
        verbose_name = 'Book in shop'
        verbose_name_plural = 'Books in shops'
        unique_together = ('book', 'shop')
        ordering = ['copies_in_stock']

    def __str__(self):
        return 'Store: {}, Book: {}'.format(self.shop.name, self.book.title)


