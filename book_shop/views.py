# Create your views here.
from django.http import Http404
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.generics import RetrieveAPIView, get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from book_shop.models import Publisher, Shop, BookInShop
from book_shop.serializers import PublisherShopSerializer, SoldBooksSerializer


class PublisherBooksShops(RetrieveAPIView):
    serializer_class = PublisherShopSerializer
    queryset = Publisher.objects.all()


class MarkBookAsSold(APIView):
    def post(self, request, *args, **kwargs):
        shop_id = kwargs.get('shop_id')
        book_id = kwargs.get('book_id')
        shop = get_object_or_404(Shop, id=shop_id)
        try:
            book_in_shop = BookInShop.objects.get(shop=shop, book_id=book_id)
        except BookInShop.DoesNotExist:
            return Response(data={'detail': 'There is no such book in this shop'}, status=status.HTTP_404_NOT_FOUND)
        serializer = SoldBooksSerializer(data=request.data)
        if serializer.is_valid():
            count = serializer.validated_data.get('count')
            if count > book_in_shop.copies_in_stock:
                raise ValidationError('There is no such amount of books in this shop')
            book_in_shop.copies_in_stock -= count
            book_in_shop.save()
            shop.books_sold += count
            shop.save()
            return Response(status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)