from rest_framework import serializers

from book_shop.models import Shop, Book, Publisher, BookInShop


class BookInShopSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source='book.id')
    title = serializers.CharField(source='book.title')

    class Meta:
        model = BookInShop
        fields = ('id', 'title', 'copies_in_stock')


class ShopsSerializer(serializers.ModelSerializer):
    books_in_stock = BookInShopSerializer(source='bookinshop_set', many=True)
    books_sold_count = serializers.IntegerField(source='books_sold')

    class Meta:
        model = Shop
        fields = ('id', 'name', 'books_sold_count', 'books_in_stock')


class PublisherShopSerializer(serializers.ModelSerializer):
    shops = serializers.SerializerMethodField()

    def get_shops(self, publisher):
        shops = Shop.objects.filter(books__publisher=publisher, bookinshop__copies_in_stock__gte=1)
        serializer = ShopsSerializer(instance=shops, many=True)
        return serializer.data

    class Meta:
        model = Publisher
        fields = ('shops',)


class SoldBooksSerializer(serializers.Serializer):
    count = serializers.IntegerField()
