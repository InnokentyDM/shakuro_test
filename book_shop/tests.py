# Create your tests here.
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient

from book_shop.models import Publisher, Book, Shop, BookInShop


class BookShopBaseTest(TestCase):
    def setUp(self):
        self.client = APIClient()

    @classmethod
    def setUpTestData(cls):
        cls.publisher1 = Publisher.objects.create(name='Test1')
        cls.publisher2 = Publisher.objects.create(name='Test2')
        cls.publisher3 = Publisher.objects.create(name='Test3')
        cls.book1 = Book.objects.create(title='Test book 1', publisher=cls.publisher1)
        cls.book2 = Book.objects.create(title='Test book 2', publisher=cls.publisher2)
        cls.book3 = Book.objects.create(title='Test book 3', publisher=cls.publisher3)
        cls.shop1 = Shop.objects.create(name='Amazon')
        cls.shop2 = Shop.objects.create(name='LitRes')
        cls.shop3 = Shop.objects.create(name='BookShop')
        cls.book_in_shop_1 = BookInShop.objects.create(book=cls.book1, shop=cls.shop1, copies_in_stock=100)
        # cls.book_in_shop_2 = BookInShop.objects.create(book=cls.book2, shop=cls.shop2, copies_in_stock=100)
        cls.book_in_shop_3 = BookInShop.objects.create(book=cls.book3, shop=cls.shop3, copies_in_stock=100)


class TestPublisherBookList(BookShopBaseTest):

    def test_publisher_shops_list(self):
        url = reverse('publisher.shops_list', kwargs={'pk': self.publisher1.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        data = response.data['shops']
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0].get('id'), 1)
        self.assertEqual(data[0].get('name'), 'Amazon')
        self.assertEqual(data[0].get('books_sold_count'), 0)
        self.assertEqual(len(data[0].get('books_in_stock')), 1)
        book_data = data[0].get('books_in_stock')[0]
        self.assertEqual(book_data.get('id'), 1)
        self.assertEqual(book_data.get('title'), 'Test book 1')
        self.assertEqual(book_data.get('copies_in_stock'), 100)

    def test_publisher_shops_empty_list(self):
        url = reverse('publisher.shops_list', kwargs={'pk': self.publisher2.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        data = response.data['shops']
        self.assertEqual(len(data), 0)


class TestMarkBooksAsSold(BookShopBaseTest):
    def test_mark_books_as_sold(self):
        url = reverse('shop.mark_book_as_sold', kwargs={'shop_id': self.shop1.id, 'book_id': self.book1.id})
        payload = {
            'count': 2
        }
        response = self.client.post(url, data=payload)
        self.assertEqual(response.status_code, 200)
        self.shop1.refresh_from_db()
        self.assertEqual(self.shop1.books_sold, 2)
        self.book_in_shop_1.refresh_from_db()
        self.assertEqual(self.book_in_shop_1.copies_in_stock, 98)

    def test_mark_books_as_sold_shop_not_found(self):
        url = reverse('shop.mark_book_as_sold', kwargs={'shop_id': 222, 'book_id': self.book1.id})
        payload = {
            'count': 2
        }
        response = self.client.post(url, data=payload)
        self.assertEqual(response.status_code, 404)

    def test_mark_books_as_sold_there_is_no_such_book_in_shop(self):
        url = reverse('shop.mark_book_as_sold', kwargs={'shop_id': self.shop1.id, 'book_id': 222})
        payload = {
            'count': 2
        }
        response = self.client.post(url, data=payload)
        self.assertEqual(response.status_code, 404)
        data = response.data['detail']
        self.assertEqual(data, 'There is no such book in this shop')

    def test_mark_books_as_sold_wrong_count(self):
        url = reverse('shop.mark_book_as_sold', kwargs={'shop_id': self.shop1.id, 'book_id': self.book1.id})
        payload = {
            'count': 200
        }
        response = self.client.post(url, data=payload)
        self.assertEqual(response.status_code, 400)
        data = response.data[0]
        self.assertEqual(data, 'There is no such amount of books in this shop')